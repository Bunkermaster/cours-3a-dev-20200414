PHP & Symfony
===

# Install

## Composer

https://getcomposer.org/download/

Download the phar in your local project directory.

## Project

From project directory :

```bash
composer install
```
# Composer

Initialize `composer.json`.

```composer init```

Generate composer autoload files.

```composer dump-autoload```

Show all dependencies.

```composer show```

Add a production dependency.

```composer require```

Add a development dependency.

```composer require --dev```

# Symfony

https://symfony.com/doc/current/setup.html

# References

## RFC
https://wiki.php.net/rfc/php8

## Nikita
https://twitter.com/nikita_ppv?lang=en

## Algo
http://www.rosettacode.org/wiki/Rosetta_Code

## Standards PHP
https://www.php-fig.org/psr/psr-12/

## For fun
https://gitlab.com/Bunkermaster/presentationafup

https://www.php-fig.org/psr/psr-3/

https://www.php.net/manual/en/language.oop5.traits.php

https://en.wikipedia.org/wiki/Cyclomatic_complexity

https://github.com/Bunkermaster/h3-algo-semaine-3

