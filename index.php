<?php

use App\goodguys\SaluteInterface;
use App\goodguys\Stormtrooper;
use App\significantlylessgoodguys\Stormtrooper as Shtormtrooper;

require_once "vendor/autoload.php";

echo Stormtrooper::getCount().PHP_EOL;
$st = new Stormtrooper('TK-101');
$sht = new Shtormtrooper();

function saluteMe(SaluteInterface $salute): void
{
    echo $salute->salute();
}

saluteMe($st);
saluteMe($sht);
