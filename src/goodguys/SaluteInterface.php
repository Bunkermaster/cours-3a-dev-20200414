<?php

namespace App\goodguys;

interface SaluteInterface
{
    /**
     * Salutes the caller
     *
     * @return string
     */
    public function salute(): string;
}