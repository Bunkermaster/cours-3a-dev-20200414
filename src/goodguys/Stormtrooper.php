<?php
namespace App\goodguys;

class Stormtrooper extends Trooper implements SaluteInterface
{
    use PewpewTrait;

    private static int $count = 0;

    /**
     * Stormtrooper constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        parent::__construct($name);
        ++static::$count;
        $this->introduction();
    }

    public function __destruct()
    {
        --static::$count;
        echo $this->name." : Why did you kill me... arrghhhh".PHP_EOL;
    }

    protected function introduction(): void
    {
        echo $this->name." : Hi, my name is ".PHP_EOL;
    }

    /**
     * @return int
     */
    public static function getCount(): int
    {
        return self::$count;
    }

    public function salute(): string
    {
        return $this->getName()." : Salut".PHP_EOL;
    }
}
