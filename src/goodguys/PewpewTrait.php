<?php

namespace App\goodguys;

trait PewpewTrait
{
    public function pewpew(): void
    {
        echo "Pew pew";
    }
}
