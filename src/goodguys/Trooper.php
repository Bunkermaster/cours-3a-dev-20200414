<?php

namespace App\goodguys;

abstract class Trooper
{
    /**
     * @var string
     */
    protected string $name;

    /**
     * Trooper constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    protected abstract function introduction(): void;
}
